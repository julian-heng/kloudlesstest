﻿namespace KloudlessCloudInfo
{
    public class Dropbox : IKloudless
    {
        public Dropbox(string clientID) : base(clientID)
        {
        }

        public override string OauthAddress => "https://api.kloudless.com/v1/oauth/services/dropbox";
    }
}s