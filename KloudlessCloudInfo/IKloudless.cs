﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web;

namespace KloudlessCloudInfo
{
    public abstract class IKloudless
    {
        //private const string OAUTH_BASE_ADDRESS = "https://api.kloudless.com/v1/oauth/services/dropbox";
        
        private readonly string clientID = null;
        private string token = null;
        
        public abstract string OauthAddress { get; }

        public IKloudless(string clientID)
        {
            this.clientID = clientID;
            this.clientID = GetToken();
        }

        private string GetToken()
        {
            string token = null;
            var authUriBuilder = new UriBuilder(OauthAddress);
            var query = HttpUtility.ParseQueryString(authUriBuilder.Query);
            query["client_id"] = clientID;
            query["response_type"] = "token";
            query["state"] = "CSRF_PREVENTION_TOKEN";
            query["redirect_uri"] = "urn:ietf:wg:oauth:2.0:oob";
            query["modifiers"] = "normal";
            authUriBuilder.Query = query.ToString();
            var authUrl = authUriBuilder.ToString();

            Console.WriteLine(authUrl);
            Console.Write("Enter Token> ");
            token = Console.ReadLine();
            return token;
        }
    }
}