﻿namespace KloudlessCloudInfo
{
    public class GoogleDrive : IKloudless
    {
        public GoogleDrive(string clientID) : base(clientID)
        {
        }

        public override string OauthAddress => "https://api.kloudless.com/v1/oauth/services/gdrive";
    }
}